//
//  ReuableInvestmentDisplay.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 28/04/2022.
//

import UIKit

class ReusableInvestmentDisplay: UIView {
  
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var companyNameLabel: UILabel!
  @IBOutlet weak var tyoeOfInvestmentLabel: UILabel!
  @IBOutlet weak var dateInvestedLabel: UILabel!
  @IBOutlet weak var amountInvestedLabel: UILabel!
  @IBOutlet weak var investmentDurationLabel: UILabel!
  @IBOutlet weak var returnsAmountLabel: UILabel!
  @IBOutlet weak var dateOfReturns: UILabel!
  @IBOutlet weak var nextPaymentLabel: UILabel!
  @IBOutlet weak var remainingPaymentsLabel: UILabel!
  
  let nibName = "ResuableInvestmentDisplayView"
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    commonInit()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  func commonInit() {
    guard let view = loadViewFromNib() else { return }
    view.frame = self.bounds
    self.addSubview(view)
    contentView = view
  }
  
  func loadViewFromNib() -> UIView? {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: nibName, bundle: bundle)
    return nib.instantiate(withOwner: self, options: nil).first as? UIView
  }
}
