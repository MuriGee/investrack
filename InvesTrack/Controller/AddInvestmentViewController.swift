//
//  AddInvestmentViewController.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 08/03/2022.
//

import UIKit

final class AddInvestmentViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet private var companyTextField: UITextField!
  @IBOutlet private var nameTextField: UITextField!
  @IBOutlet private var amountTextField: UITextField!
  @IBOutlet private var dateTextField: UITextField!
  @IBOutlet private var expectedPaymentsTextField: UITextField!
  @IBOutlet private var returnsAmountTextField: UITextField!
  @IBOutlet private var returnsStartDateTextField: UITextField!
  
  // MARK: - Variables
  var datePicker: UIDatePicker!
  var dateFormatter: DateFormatter!
  var investment: InvestmentItem?
  
  // MARK: - UIViewControllerEvents
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Add Investment"
    navigationController?.navigationBar.prefersLargeTitles = true
    
    setUpVariables()
  }
  
  // MARK: - IBAction Functions
  @IBAction func addButtonTapped(_ sender: Any) {
    checkIsEmpty()
  }
}

// MARK: - UIDatePicker Functions
extension AddInvestmentViewController {
  private func setUpVariables() {
    datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    datePicker.preferredDatePickerStyle = .wheels
    
    let toolbar = UIToolbar()
    toolbar.sizeToFit()
    
    toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelTapped)), UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneTapped))], animated: true)
    
    dateTextField.inputAccessoryView = toolbar
    returnsStartDateTextField.inputAccessoryView = toolbar
    
    dateTextField.inputView = datePicker
    returnsStartDateTextField.inputView = datePicker
    
    dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MMMM yyyy"
  }
  
  @objc func cancelTapped() {
    view.endEditing(true)
  }
  
  @objc func doneTapped() {
    if dateTextField.isFirstResponder {
      dateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    if returnsStartDateTextField.isFirstResponder {
      returnsStartDateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    view.endEditing(true)
  }
}


// MARK: - Validation Functions
extension AddInvestmentViewController {
  //TODO: - Use Date Picker instead of letting users type.
  private func checkIsEmpty() {
    guard
      let companyLabel = companyTextField.placeholder,
      let nameLabel = nameTextField.placeholder,
      let amountLabel = amountTextField.placeholder,
      let expectPaymentsLabel = expectedPaymentsTextField.placeholder,
      let returnsAmountLabel = returnsAmountTextField.placeholder
    else { return }
    guard
      let company = companyTextField.text,
      !company.isEmpty
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: companyLabel))
      return
    }
    guard
      let name = nameTextField.text,
      !name.isEmpty
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: nameLabel))
      return
    }
    guard
      let amount = amountTextField.text,
      !amount.isEmpty,
      Double(amount) != nil
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .invalidNumber, label: amountLabel) + "the amount you invested and try again.")
      return
    }
    guard
      let date = dateTextField.text,
      !date.isEmpty
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .invalidDate, label: "Date Invested"))
      return
    }
    guard
      let expectedPayments = expectedPaymentsTextField.text,
      !expectedPayments.isEmpty,
      Int(expectedPayments) != nil
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .invalidNumber, label: expectPaymentsLabel) + "how many payments you are expecting to receive and try again.")
      return
    }
    guard
      let returnsAmount = returnsAmountTextField.text,
      !returnsAmount.isEmpty,
      Double(returnsAmount) != nil
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .invalidNumber, label: returnsAmountLabel)  + "how much you are going to be receivng in return and try again.")
      return
    }
    guard
      let returnsStartDate = returnsStartDateTextField.text,
      !returnsStartDate.isEmpty
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: errorMessage(errorCode: .invalidDate, label: "Returns Start Date"))
      return
    }
    checkDates()
  }
  
  private func checkDates() {
    guard
      let dateInvested = dateFormatter.date(from: dateTextField.text!),
      let returnsDate = dateFormatter.date(from: returnsStartDateTextField.text!),
        dateInvested <= returnsDate
    else {
      showErrorMessage(errorTitle: .addingFailed, errorMessage: "Returns Start Date cannot be before Date Invested.")
      return
    }
    addToInvestmentObject()
  }
  
  private func addToInvestmentObject() {
    investment = InvestmentItem(companyName: companyTextField.text!,
                                typeOfInvestment: nameTextField.text!,
                                amountInvested: Double(amountTextField.text!),
                                dateInvested: dateTextField.text!,
                                expectedPayments: Int(expectedPaymentsTextField.text!),
                                returnsAmounts: Double(returnsAmountTextField.text!),
                                returnsStartDate: returnsStartDateTextField.text!)
    performSegue(withIdentifier: "previewInvestment", sender: self)
  }
  
  private func clearTextFields() {
    nameTextField.text = ""
    amountTextField.text = ""
    dateTextField.text = ""
    expectedPaymentsTextField.text = ""
    returnsAmountTextField.text = ""
    returnsStartDateTextField.text = ""
  }
}

// MARK: - Navigation
extension AddInvestmentViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let destinationVC = segue.destination as! InvestmentPreviewViewController
    destinationVC.investment = investment
    destinationVC.addInvestmentBool = true
  }
}
