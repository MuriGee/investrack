//
//  ViewController.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 15/02/2022.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  // MARK: - Variables
  private var db = Firestore.firestore()
  
  // MARK: - UIViewControllerEvents
  override func viewDidLoad() {
    super.viewDidLoad()
    authListener()
  }
  
  // MARK: - Action Functions
  @IBAction func signInButtonTapped(_ sender: Any) {
    signIn()
  }
  
  @IBAction func signUpButtonTapped(_ sender: Any) {
    clearTextFields()
  }
}

// MARK: - Private Functions
extension SignInViewController {
  private func authListener() {
    Auth.auth().addStateDidChangeListener { auth, user in
      if user != nil {
        self.db.collection("users").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
          if let document = document, document.exists {
            self.performSegue(withIdentifier: "loggedIn", sender: self)
            self.clearTextFields()
          }
        }
      }
    }
  }
  
  private func signIn() {
    Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
      if let error = error, user == nil {
        let alert = UIAlertController(title: "Sign In Failed", message: error.localizedDescription, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        
        self.present(alert, animated: true, completion: nil)
      } else { self.clearTextFields() }
    }
  }
  
  private func clearTextFields() {
    emailTextField.text = ""
    passwordTextField.text = ""
  }
}

