//
//  SignUpViewController.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 26/02/2022.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var firstNameTextField: UITextField!
  @IBOutlet weak var surnameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var secPasswordTextField: UITextField!
  
  // MARK: - Variables
  var db = Firestore.firestore()
  var passwordAlertShown = false
  
  // MARK: - UIViewControllerEvents
  override func viewDidLoad() {
    super.viewDidLoad()
    passwordTextField.delegate = self
  }
  
  // MARK: - IBAction Functions
  @IBAction func signUpButtonTapped(_ sender: Any) {
    checkTextfields()
  }
  
  @IBAction func signInButtonTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
}

// MARK: - Firebase Functions
extension SignUpViewController {
  private func createUser(email: String, password: String) {
    Auth.auth().createUser(withEmail: email, password: password) { user, error in
      if error == nil {
        print("Success")
        self.createAccountInDatabase()
      } else {
        self.showErrorMessage(errorTitle: .signUpFailed, errorMessage: error!.localizedDescription)
      }
    }
  }
  
  private func createAccountInDatabase() {
    db.collection("users").document(Auth.auth().currentUser!.uid).setData([
      "name" : self.firstNameTextField.text! + " " + self.surnameTextField.text!,
      "email" : self.emailTextField.text!,
      "uid" : Auth.auth().currentUser?.uid as Any
    ]) { err in
      if let err = err {
        print("Error writing document: \(err)")
      } else {
        print("Document successfully written!")
      }
    }
  }
}
// MARK: - Validity Check Functions
extension SignUpViewController {
  private func checkTextfields() {
    guard
      let firstName = firstNameTextField.text,
      let firstNameLabel = firstNameTextField.placeholder,
      let surname = surnameTextField.text,
      let surnameLabel = surnameTextField.placeholder,
      let email = emailTextField.text,
      let emailLabel = emailTextField.placeholder,
      let password = passwordTextField.text,
      let secPassword = secPasswordTextField.text
    else {
      return
    }
    guard
      !firstName.isEmpty
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: firstNameLabel))
      return
    }
    guard
      !surname.isEmpty
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: surnameLabel))
      return
    }
    guard
      !email.isEmpty
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: emailLabel))
      return
    }
    guard
      !password.isEmpty
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: "Password"))
      return
    }
    guard
      !secPassword.isEmpty
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: errorMessage(errorCode: .emptyFields, label: "Re-Enter Password"))
      return
    }
    checkPasswords()
  }
  
  private func checkPasswords() {
    let numSet = CharacterSet(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
    let uppercase = CharacterSet.uppercaseLetters
    let lowercase = CharacterSet.lowercaseLetters
    guard
      let password = passwordTextField.text,
      let secPassword = secPasswordTextField.text
    else { return }
    guard
      password.count > 5,
      password.rangeOfCharacter(from: numSet) != nil,
      password.rangeOfCharacter(from: uppercase) != nil,
      password.rangeOfCharacter(from: lowercase) != nil
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: "Make sure Password is a combination of six or more characters including at least 1 Upper case letter, 1 lower case letter and 1 number and try again. (symbols can be included)")
      return
    }
    guard
      password == secPassword
    else {
      showErrorMessage(errorTitle: .signUpFailed, errorMessage: "Passwords do not match")
      return
    }
    createUser(email: emailTextField.text!, password: passwordTextField.text!)
  }
}

// MARK: - PasswordTextFieldDelegate
extension SignUpViewController: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    if passwordAlertShown == false {
      let ac = UIAlertController(title: "Password Requirements", message: "Please enter a combination of six or more characters including at least 1 Upper case letter, 1 lower case letter and 1 number. (symbols can be included)", preferredStyle: .alert)
      ac.addAction(UIAlertAction(title: "OK", style: .default) { action in
        self.passwordAlertShown = true
      })
      present(ac, animated: true)
    }
  }
}
