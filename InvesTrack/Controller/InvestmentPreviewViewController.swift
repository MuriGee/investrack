//
//  InvestmentPreviewViewController.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 17/02/2022.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class InvestmentPreviewViewController: UIViewController {
  // MARK: - IBOutlets
  
  @IBOutlet weak var contentView: ReusableInvestmentDisplay!
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Variables
  var investment: InvestmentItem!
  var paymentSchedule = [Date]()
  var paymentsReceived = [String]()
  var nextPayment = String()
  var db: Firestore!
  var addInvestmentBool = true
  
  // MARK: - UIViewControllerEvents
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.dataSource = self
    // Do any additional setup after loading the view.
    navigationItem.largeTitleDisplayMode = .never
    if addInvestmentBool {
      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addToFirestore))
    }
    setUpVariables()
    loadData()
  }
  
  private func setUpVariables() {
    paymentSchedule = createPaymentScheduleArray(expectedPayment: investment.expectedPayments!, startDateStr: investment.returnsStartDate!)
    
    paymentsReceived = createPaymentsReceivedArray(paymentShedule: paymentSchedule)
  }
  
  private func loadData() {
    contentView.companyNameLabel.text = investment.companyName
    contentView.tyoeOfInvestmentLabel.text = investment.typeOfInvestment
    contentView.dateInvestedLabel.text = investment.dateInvested
    contentView.amountInvestedLabel.text = "\(investment.amountInvested!)"
    
    contentView.investmentDurationLabel.text = "\(calculateDuration(dateIvested: stringToDate(dateString: investment.dateInvested!), schedule: paymentSchedule)) months"
    
    contentView.returnsAmountLabel.text = "\(investment.returnsAmounts!)"
    
    contentView.dateOfReturns.text = "\(dayOfReturns(date: stringToDate(dateString: investment.returnsStartDate!))) of the month."
    
    nextPayment = dateToString(date: nextPay(paySchedule: paymentSchedule, receivedPayments: paymentsReceived))
    contentView.nextPaymentLabel.text = nextPayment
    
    contentView.remainingPaymentsLabel.text = "\(paymentSchedule.count - paymentsReceived.count)"
  }
}

// MARK: - Private Functions
extension InvestmentPreviewViewController {
  private func dateToString(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "EEEE d MMMM yyyy"
    let dateString = dateFormatter.string(from: date)
    return dateString
  }

  private func stringToDate(dateString: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "d MMMM yyyy"
    let date = dateFormatter.date(from: dateString)!
    return date
  }
  
  private func calculateDuration(dateIvested: Date, schedule: [Date]) -> Int {
    let lastPayment = schedule.suffix(1).first
    let duration = Calendar.current.dateComponents([.month], from: dateIvested, to: lastPayment!)
    return duration.month!
  }
  
  private func dayOfReturns(date: Date) -> String {
    let dayComponenet = Calendar.current.component(.day, from: date)
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .ordinal
    let day = numberFormatter.string(from: dayComponenet as NSNumber)!
    return day
  }
  
  private func nextPay(paySchedule: [Date], receivedPayments: [String]) -> Date {
    let suffix = paySchedule.count - receivedPayments.count
    let remainingPayments = paySchedule.suffix(suffix)
    let nextPayment = remainingPayments.first
    return nextPayment!
  }
  
  private func createPaymentScheduleArray(expectedPayment: Int, startDateStr: String) -> Array<Date> {
    var schedule = [Date]()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MMMM yyyy"
    let startDate = dateFormatter.date(from: startDateStr)
    
    for i in 0...expectedPayment - 1 {
      let payDates = Calendar.current.date(byAdding: .month, value: i, to: startDate!)
      schedule.append(payDates!)
    }
    return schedule
  }

  private func createPaymentsReceivedArray(paymentShedule: [Date]) -> Array<String> {
    var receivedPayments = [Date]()
    var receivedPaymentsString = [String]()
    let today = Date()
    for date in paymentShedule {
      if date < today {
        receivedPayments.append(date)
      }
    }
    for day in receivedPayments {
      receivedPaymentsString.append(dateToString(date: day))
    }
    return receivedPaymentsString
  }
}

// MARK: - Firebase Functions
extension InvestmentPreviewViewController {
  @objc func addToFirestore() {
    db = Firestore.firestore()
    do {
      try db.collection("users").document(Auth.auth().currentUser!.uid).collection("Portfolio").document(UUID().uuidString).setData(from: investment)
    } catch {
      fatalError("Could not upload")
    }
    performSegue(withIdentifier: "investmentAdded", sender: self)
  }
}

// MARK: - TableViewDataSource
extension InvestmentPreviewViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if paymentsReceived.count == 0 {
      return 1
    } else {
      return paymentsReceived.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
    var configuration = cell.defaultContentConfiguration()
    
    if paymentsReceived.count == 0 {
      configuration.text = "You haven't received any payments yet. First Payment will be on \(nextPayment)"
    } else {
      configuration.text = "\(paymentsReceived[indexPath.row]) for \(String(format: "%.2f", investment.returnsAmounts!))"
    }
    cell.contentConfiguration = configuration
    return cell
  }
}
