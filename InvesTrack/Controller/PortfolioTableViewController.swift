//
//  PortfolioTableViewController.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 04/03/2022.
//

import UIKit
import Firebase

class PortfolioTableViewController: UITableViewController {
  
  // MARK: - Variables
  var db: Firestore!
  var investments = [InvestmentItem]()
  var investment = InvestmentItem()
  
  // MARK: - UIViewControllerEvents
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Portfolio"
    navigationController?.navigationBar.prefersLargeTitles = true
    loadData()
  }
  
  // MARK: - IBActionFunctions
  @IBAction func signOutTapped(_ sender: Any) {
    try! Auth.auth().signOut()
    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
  }
}

// MARK: - Private Funcitons
extension PortfolioTableViewController {
  private func loadData() {
    db = Firestore.firestore()
    db.collection("users").document(Auth.auth().currentUser!.uid).collection("Portfolio").addSnapshotListener { querySnapshot, error in
      if querySnapshot!.isEmpty {
        let ac = UIAlertController(title: "No Investments Added", message: "You currently don't have any investments added. Please add to see them displayed.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(ac, animated: true)
      } else {
        self.investments = (querySnapshot?.documents.compactMap {
          return try? $0.data(as: InvestmentItem.self)
        })!
        self.tableView.reloadData()
      }
    }
  }
}

// MARK: - UITableViewDataSource
extension PortfolioTableViewController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if investments.count == 0 {
      return 1
    } else {
    return investments.count
    }
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    var configuration = cell.defaultContentConfiguration()
    if investments.count == 0 {
      configuration.text = "No Investments to display at the moment. Please Add."
    } else {
      investment = investments[indexPath.row]
      configuration.text = "\(investment.typeOfInvestment!) | \(String(format: "$%.2f", investment.amountInvested!)) | \(investment.dateInvested!) | \(String(format: "$%.2f", investment.returnsAmounts!))"
    }
    cell.contentConfiguration = configuration
    
    return cell
  }
}

// MARK: - UITableViewDelagate
extension PortfolioTableViewController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    investment = investments[indexPath.row]
    performSegue(withIdentifier: "displayInvestment", sender: self)
  }
}

// MARK: - Navigation
extension PortfolioTableViewController {
  // Destination for unwinding Segue.
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "displayInvestment" {
      let destinationVC = segue.destination as! InvestmentPreviewViewController
      destinationVC.investment = investment
      destinationVC.addInvestmentBool = false
    }
  }
  
  @IBAction func unwind( _segue: UIStoryboardSegue) { }
}


