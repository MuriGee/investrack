//
//  AlertExtension.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 25/04/2022.
//

import UIKit

enum ErrorCode {
  case invalidEmail
  case invalidNumber
  case invalidDate
  case emptyFields
}

enum ErrorTitle {
  case signUpFailed
  case addingFailed
}

extension UIViewController {
  func errorMessage(errorCode: ErrorCode, label: String) -> String {
    var message = String()
    switch errorCode {
    case .invalidEmail:
      message = "Please enter valid email address"
    case .invalidNumber:
      message = "Make sure '\(label)' text field has been filled with "
    case .invalidDate:
      message = "Make sure the '\(label)' text field is filled in and try again."
    case .emptyFields:
      message = "Make sure '\(label)' text field is filled in and try again."
    }
    return message
  }
  
  func showErrorMessage(errorTitle: ErrorTitle, errorMessage: String) {
    var titleMessage = String()
    switch errorTitle {
    case .signUpFailed:
      titleMessage = "Sign Up Failed"
    case .addingFailed:
      titleMessage = "Adding Failed"
    }
    
    let ac = UIAlertController(title: titleMessage, message: errorMessage, preferredStyle: .alert)
    ac.addAction(UIAlertAction(title: "OK", style: .default))
    present(ac, animated: true)
  }
}
