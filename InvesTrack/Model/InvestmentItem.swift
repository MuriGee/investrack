//
//  InvestmentItem.swift
//  InvesTrack
//
//  Created by Muri Gumbodete on 28/04/2022.
//

import Foundation

struct InvestmentItem: Codable {
  var companyName: String?
  var typeOfInvestment: String?
  var amountInvested: Double?
  var dateInvested: String?
  var expectedPayments: Int?
  var returnsAmounts: Double?
  var returnsStartDate: String?
}
