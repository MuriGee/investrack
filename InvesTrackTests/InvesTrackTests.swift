//
//  InvesTrackTests.swift
//  InvesTrackTests
//
//  Created by Muri Gumbodete on 15/02/2022.
//

import XCTest
@testable import InvesTrack

class InvesTrackTests: XCTestCase {
    
    var sut: SignUpViewController!
    private let firstName = "Muri"
    private let surname = "Gee"
    private let email = "muri@test.com"
    private let password = "Test321"

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = SignUpViewController()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testRegisterNewUserAtFirebaseAddsToAuthDatabase() {
        // given
        
        // when
        sut.createUser(email: email, password: password)
        
        // then
        //XCTAssertTrue(sut.test)
        //XCTAssertNil(sut.createUser(email:email, password: password))
    }
}
